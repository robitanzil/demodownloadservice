package edu.robitanzil.demodownloadservice.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import edu.robitanzil.demodownloadservice.R;

public class DownloadService extends Service {

	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	
	private NotificationManager mNM;
	private NotificationCompat.Builder builder;
	
	private File storagePath;
	private String downloadUrl;
	private String fileName;
	
	public static boolean serviceState = false;
	public static int ID = 10;

	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			showNotification();
			downloadFile();
			hideNotification();
			stopSelf(msg.arg1);
		}
	}

	@Override
	public void onCreate() {
		serviceState = true;
		HandlerThread thread = new HandlerThread("ServiceStartArguments", 1);
		thread.start();

		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("SERVICE-ONCOMMAND", "onStartCommand");

		Bundle extra = intent.getExtras();
		if (extra != null) {

			String downloadUrl = extra.getString("URL");
			String fileName = extra.getString("FILENAME");

			this.downloadUrl = downloadUrl;
			this.fileName = fileName;
		}

		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);

		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.d("SERVICE-DESTROY", "DESTORY");
		serviceState = false;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public void downloadFile() {

		downloadFile(this.downloadUrl, fileName);

	}

	void showNotification() {

		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		builder = new NotificationCompat.Builder(this);
		builder.setContentTitle("Photo Download")
		    .setContentText("Download in progress")
		    .setSmallIcon(R.drawable.ic_launcher)
		    .setAutoCancel(true)
		    .setOngoing(true);
		
		mNM.notify(ID, builder.build());
	}
	
	void hideNotification() {
		builder.setContentText("Download complete").setProgress(0, 0, false);
		
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		File file = new File(storagePath, fileName);

		System.out.println("Path = " + file.getAbsolutePath());
		System.out.println("Name = " + file.getName());
		
		intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "image/*");
		
		PendingIntent pending = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pending);
	        

		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mNM.notify(ID, builder.build());
	}

	public void downloadFile(String fileURL, String fileName) {
		try {
			URL url = new URL (fileURL);
			InputStream input = url.openStream();
			try {
			    storagePath = new File(Environment.getExternalStorageDirectory() + "/TheLounge");
			    if (!storagePath.exists() && !storagePath.isDirectory()) {
			    	storagePath.mkdir();
				}
			    
			    OutputStream output = new FileOutputStream (new File(storagePath, fileName));
			    try {
			        byte[] buffer = new byte[1024];
			        int bytesRead = 0;
			        while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
			            output.write(buffer, 0, bytesRead);
			        }
			    } finally {
			        output.close();
			    }
			} finally {
			    input.close();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}