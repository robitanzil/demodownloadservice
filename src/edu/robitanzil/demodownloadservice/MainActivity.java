package edu.robitanzil.demodownloadservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import edu.robitanzil.demodownloadservice.utils.DownloadService;

public class MainActivity extends Activity {

	private Context context;
	private Button btnDownload;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		drawView();
	}

	private void drawView() {
		btnDownload = (Button) findViewById(R.id.am_button_download);
		btnDownload.setOnClickListener(downloadListener);
	}

	private OnClickListener downloadListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Toast.makeText(context, "Service Started", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(context, DownloadService.class);
			intent.putExtra("FILENAME", "shirt.jpg");
			intent.putExtra("URL", "http://soccerlens.com/files/2013/03/Chelsea-13-14-Home-Shirt-Leaked.jpg");
			startService(intent);
		}
	};

}
